# Sell TM Android Dev Assignment
Basic functionality of bus search and result page

![](images/booking.jpg)
![](images/search_result.jpg)

###API
| KEY | VALUE |
| ------------- | ------------- |
| Type | Post   |
| URL  | https://sandbox.selltm.com/api/v1/buses/query   |

###Header
|  KEY | VALUE |
| ------------- | ------------- |
| Authorization   | Bearer 3ca71bf062fa841007f93a1c0613c3f611969b0d3e40886ce4817a872ddb86ea  |

###Form Data
|  KEY | VALUE |TYPE|
| ------------- | ------------- |
| source   | 3  |fixed value|
| destination   | 6  |fixed value|
| doj   | 2019-10-30  | variable according to date picked|

#App Show Case
![](images/app_show_case.gif = 720x)