package com.pallaw.selltmassignment.Base


import com.google.gson.annotations.SerializedName

data class BaseResponse(
    @SerializedName("message")
    var message: String = "",
    @SerializedName("status")
    var status: String = ""
)