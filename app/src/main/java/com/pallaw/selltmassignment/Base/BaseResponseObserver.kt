package com.pallaw.selltmassignment.Base

import com.google.gson.JsonSyntaxException
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.net.ConnectException

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
open abstract class BaseResponseObserver<T> : Observer<T> {

    companion object {
        var TAG_CONNECTION_LOST = "Connection lost"
        var TAG_RESPONSE_CHANGED = "Response has been changed"
    }

    override fun onComplete() {

    }

    override fun onSubscribe(disposable: Disposable) {

    }

    /**
     * Fetch the response and pass to onResponse
     */
    override fun onNext(response: T) {
        onResponse(response)
    }

    /**
     * Handle the error and pass it to onServerError
     */
    override fun onError(error: Throwable) {

        when {

            (error is ConnectException) -> {
                onServerError(BaseResponse().apply { message = TAG_CONNECTION_LOST })
                return
            }

            (error is JsonSyntaxException) -> {
                onServerError(BaseResponse().apply { this.message = TAG_RESPONSE_CHANGED })
                error.printStackTrace()
                return
            }

            else -> {
                onServerError(BaseResponse().apply { message = error.message.toString() })
            }
        }

    }

    abstract fun onResponse(response: T)

    abstract fun onServerError(baseResponse: BaseResponse)
}