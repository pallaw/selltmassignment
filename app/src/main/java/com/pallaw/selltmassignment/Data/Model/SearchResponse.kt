package com.pallaw.selltmassignment.Data.Model


import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import java.io.Serializable

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
data class SearchResponse(
    @SerializedName("data")
    var data: JsonElement,
    @SerializedName("message")
    var message: String = "",
    @SerializedName("status")
    var status: String = ""
) : Serializable {

    var dataSafe: ArrayList<Data> = arrayListOf<Data>()
        get() {
            if (null != data) {
                if (data.isJsonArray) {
                    val founderListType = object : TypeToken<ArrayList<Data>>() {}.type
                    var dataList: ArrayList<Data> =
                        Gson().fromJson(data, founderListType)

                    return dataList

                } else {
                    val data = Gson().fromJson(data, Data::class.java)
                    return arrayListOf<Data>().apply { add(data) }
                }
            } else {
                return arrayListOf<Data>()
            }
        }

    data class Data(
        @SerializedName("operator")
        var `operator`: String = "",
        @SerializedName("AC")
        var aC: String = "",
        @SerializedName("arrivalTime")
        var arrivalTime: String = "",
        @SerializedName("availCatCard")
        var availCatCard: String = "",
        @SerializedName("availSrCitizen")
        var availSrCitizen: String = "",
        @SerializedName("availableSeats")
        var availableSeats: String = "",
        @SerializedName("avlWindowSeats")
        var avlWindowSeats: String = "",
        @SerializedName("boardingTimes")
        var boardingTimes: JsonElement,
        @SerializedName("bookable")
        var bookable: String = "",
        @SerializedName("bpDpSeatLayout")
        var bpDpSeatLayout: String = "",
        @SerializedName("busImageCount")
        var busImageCount: String = "",
        @SerializedName("busRoutes")
        var busRoutes: String = "",
        @SerializedName("busType")
        var busType: String = "",
        @SerializedName("busTypeId")
        var busTypeId: String = "",
        @SerializedName("cancellationPolicy")
        var cancellationPolicy: String = "",
        @SerializedName("cpId")
        var cpId: String = "",
        @SerializedName("departureTime")
        var departureTime: String = "",
        @SerializedName("destination")
        var destination: String = "",
        @SerializedName("doj")
        var doj: String = "",
        @SerializedName("dropPointMandatory")
        var dropPointMandatory: String = "",
        @SerializedName("droppingTimes")
        var droppingTimes: JsonElement,
        @SerializedName("fareDetails")
        var fareDetails: JsonElement,
        @SerializedName("fares")
        var fares: JsonElement,
        @SerializedName("flatComApplicable")
        var flatComApplicable: String = "",
        @SerializedName("gdsCommission")
        var gdsCommission: String = "",
        @SerializedName("id")
        var id: String = "",
        @SerializedName("idProofRequired")
        var idProofRequired: String = "",
        @SerializedName("liveTrackingAvailable")
        var liveTrackingAvailable: String = "",
        @SerializedName("mTicketEnabled")
        var mTicketEnabled: String = "",
        @SerializedName("maxSeatsPerTicket")
        var maxSeatsPerTicket: String = "",
        @SerializedName("nonAC")
        var nonAC: String = "",
        @SerializedName("otgEnabled")
        var otgEnabled: String = "",
        @SerializedName("partialCancellationAllowed")
        var partialCancellationAllowed: String = "",
        @SerializedName("partnerBaseCommission")
        var partnerBaseCommission: String = "",
        @SerializedName("primaryPaxCancellable")
        var primaryPaxCancellable: String = "",
        @SerializedName("reschedulingPolicy")
        var reschedulingPolicy: JsonElement,
        @SerializedName("routeId")
        var routeId: String = "",
        @SerializedName("rtc")
        var rtc: String = "",
        @SerializedName("seater")
        var seater: String = "",
        @SerializedName("selfInventory")
        var selfInventory: String = "",
        @SerializedName("singleLadies")
        var singleLadies: String = "",
        @SerializedName("sleeper")
        var sleeper: String = "",
        @SerializedName("source")
        var source: String = "",
        @SerializedName("tatkalTime")
        var tatkalTime: String = "",
        @SerializedName("travels")
        var travels: String = "",
        @SerializedName("vehicleType")
        var vehicleType: String = "",
        @SerializedName("zeroCancellationTime")
        var zeroCancellationTime: String = ""
    ) {

        var boardingTimesSafe: ArrayList<BoardingTimes> = arrayListOf<BoardingTimes>()
            get() {
                if (boardingTimes.isJsonArray) {
                    val founderListType = object : TypeToken<ArrayList<BoardingTimes>>() {}.type
                    var dataList: ArrayList<BoardingTimes> =
                        Gson().fromJson(boardingTimes, founderListType)

                    return dataList

                } else {
                    val data = Gson().fromJson(boardingTimes, BoardingTimes::class.java)
                    return arrayListOf<BoardingTimes>().apply { add(data) }
                }
            }

        var droppingTimesSafe: ArrayList<DroppingTimes> = arrayListOf<DroppingTimes>()
            get() {
                if (droppingTimes.isJsonArray) {
                    val founderListType = object : TypeToken<ArrayList<DroppingTimes>>() {}.type
                    var dataList: ArrayList<DroppingTimes> =
                        Gson().fromJson(droppingTimes, founderListType)

                    return dataList

                } else {
                    val data = Gson().fromJson(droppingTimes, DroppingTimes::class.java)
                    return arrayListOf<DroppingTimes>().apply { add(data) }
                }
            }

        var reschedulingPolicySafe: ArrayList<ReschedulingPolicy> =
            arrayListOf<ReschedulingPolicy>()
            get() {
                if (reschedulingPolicy.isJsonArray) {
                    val founderListType =
                        object : TypeToken<ArrayList<ReschedulingPolicy>>() {}.type
                    var dataList: ArrayList<ReschedulingPolicy> =
                        Gson().fromJson(reschedulingPolicy, founderListType)

                    return dataList

                } else {
                    val data = Gson().fromJson(reschedulingPolicy, ReschedulingPolicy::class.java)
                    return arrayListOf<ReschedulingPolicy>().apply { add(data) }
                }
            }

        var fareDetailsSafe: ArrayList<FareDetails> = arrayListOf<FareDetails>()
            get() {
                if (fareDetails.isJsonArray) {
                    val founderListType = object : TypeToken<ArrayList<FareDetails>>() {}.type
                    var dataList: ArrayList<FareDetails> =
                        Gson().fromJson(fareDetails, founderListType)

                    return dataList

                } else {
                    val data = Gson().fromJson(fareDetails, FareDetails::class.java)
                    return arrayListOf<FareDetails>().apply { add(data) }
                }
            }


        data class BoardingTimes(
            @SerializedName("address")
            var address: String = "",
            @SerializedName("bpId")
            var bpId: String = "",
            @SerializedName("bpName")
            var bpName: String = "",
            @SerializedName("contactNumber")
            var contactNumber: String = "",
            @SerializedName("landmark")
            var landmark: String = "",
            @SerializedName("location")
            var location: String = "",
            @SerializedName("prime")
            var prime: String = "",
            @SerializedName("time")
            var time: String = ""
        )

        data class ReschedulingPolicy(
            @SerializedName("reschedulingCharge")
            var reschedulingCharge: String = "",
            @SerializedName("windowTime")
            var windowTime: String = ""
        )

        data class FareDetails(
            @SerializedName("affiliate_pays")
            var affiliatePays: String = "",
            @SerializedName("baseFare")
            var baseFare: String = "",
            @SerializedName("bookingFee")
            var bookingFee: String = "",
            @SerializedName("commision")
            var commision: Double = 0.0,
            @SerializedName("markupFareAbsolute")
            var markupFareAbsolute: String = "",
            @SerializedName("markupFarePercentage")
            var markupFarePercentage: String = "",
            @SerializedName("operatorServiceChargeAbsolute")
            var operatorServiceChargeAbsolute: String = "",
            @SerializedName("operatorServiceChargePercentage")
            var operatorServiceChargePercentage: String = "",
            @SerializedName("serviceTaxAbsolute")
            var serviceTaxAbsolute: String = "",
            @SerializedName("serviceTaxPercentage")
            var serviceTaxPercentage: String = "",
            @SerializedName("totalFare")
            var totalFare: String = ""
        )

        data class DroppingTimes(
            @SerializedName("address")
            var address: String = "",
            @SerializedName("bpId")
            var bpId: String = "",
            @SerializedName("bpName")
            var bpName: String = "",
            @SerializedName("contactNumber")
            var contactNumber: String = "",
            @SerializedName("landmark")
            var landmark: String = "",
            @SerializedName("location")
            var location: String = "",
            @SerializedName("prime")
            var prime: String = "",
            @SerializedName("time")
            var time: String = ""
        )
    }
}