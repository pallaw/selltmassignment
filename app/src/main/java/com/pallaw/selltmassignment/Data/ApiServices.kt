package com.pallaw.selltmassignment.Data

import com.pallaw.selltmassignment.Data.Model.SearchResponse
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
interface ApiServices {

    companion object {
        val BASE_URL: String = "https://sandbox.selltm.com/api/v1/"
        val TOKEN: String =
            "Bearer 3ca71bf062fa841007f93a1c0613c3f611969b0d3e40886ce4817a872ddb86ea"
    }

    @FormUrlEncoded
    @POST("buses/query")
    fun searchBus(
        @Header("Authorization") token: String,
        @Field("source") source: String,
        @Field("destination") destination: String,
        @Field("doj") dateOfJourney: String
    ): Observable<SearchResponse>
}
