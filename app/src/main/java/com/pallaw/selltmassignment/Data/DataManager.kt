package com.pallaw.selltmassignment.Data

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import com.pallaw.selltmassignment.R
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class DataManager(private val mContext: FragmentActivity) {

    private val apiServices: ApiServices

    val apiService: ApiServices
        get() {
            if (null == apiServices) {
                try {
                    throw Exception("Data Manager is not initialized \n Please use DataManager.getInstance().init() to before using getApiService")
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
            return apiServices
        }

    private val isConnected: Boolean
        get() {
            try {
                val e =
                    mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as android.net.ConnectivityManager
                val activeNetwork = e.activeNetworkInfo
                val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
                if (!isConnected) {
                    showConnectionErrorMsg()
                }
                return isConnected
            } catch (e: Exception) {
                Log.w(TAG, e.toString())
            }
            return false
        }

    init {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val httpCacheDirectory = File(mContext.cacheDir, "offlineCache")

        //10 MB
        val cache = Cache(httpCacheDirectory, (10 * 1024 * 1024).toLong())

        val httpClient = OkHttpClient.Builder().cache(cache)
            .addInterceptor(httpLoggingInterceptor)
            .addNetworkInterceptor(provideCacheInterceptor())
            .addInterceptor(provideOfflineCacheInterceptor())
            .build()

        val smwRetrofit =
            Retrofit.Builder().addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .client(httpClient)
                .baseUrl(ApiServices.BASE_URL)
                .build()

        apiServices = smwRetrofit.create(ApiServices::class.java)
    }

    private fun provideOfflineCacheInterceptor(): Interceptor {
        return Interceptor { chain ->
            var request = chain.request()

            if (!isConnected) {
                val cacheControl = CacheControl.Builder().maxStale(7, TimeUnit.DAYS)
                    .build()

                request = request.newBuilder()
                    .removeHeader(HEADER_PRAGMA)
                    .removeHeader(HEADER_CACHE_CONTROL)
                    .cacheControl(cacheControl)
                    .build()
            }

            chain.proceed(request)
        }
    }

    private fun provideCacheInterceptor(): Interceptor {
        return Interceptor { chain ->
            val response = chain.proceed(chain.request())

            val cacheControl: CacheControl

            if (isConnected) {
                cacheControl = CacheControl.Builder().maxAge(0, TimeUnit.SECONDS).build()
            } else {
                cacheControl = CacheControl.Builder().maxStale(7, TimeUnit.DAYS).build()
            }

            response.newBuilder()
                .removeHeader(HEADER_PRAGMA)
                .removeHeader(HEADER_CACHE_CONTROL)
                .header(HEADER_CACHE_CONTROL, cacheControl.toString())
                .build()
        }
    }

    private fun showConnectionErrorMsg() {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            Toast.makeText(
                mContext, mContext.resources
                    .getString(R.string.error_connection), Toast.LENGTH_LONG
            ).show()
        }
    }

    companion object {

        val HEADER_CACHE_CONTROL = "Cache-Control"
        val HEADER_PRAGMA = "Pragma"
        private val TAG = DataManager::class.java.simpleName
    }
}
