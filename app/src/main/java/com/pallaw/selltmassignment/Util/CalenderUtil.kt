package com.pallaw.selltmassignment.Util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Pallaw Pathak on 2019-09-30. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class CalenderUtil {

    fun getIncrementedDate(currentDate: String, noOfDayToBeAdded: Int): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        val date:Date = sdf.parse(currentDate)!!
        val cal:Calendar = Calendar.getInstance()
        cal.setTime(date)

        cal.add(Calendar.DAY_OF_MONTH, noOfDayToBeAdded)

        return sdf.format(cal.time)
    }

    /**
     * returns date as "EEE',' dd MMM"
     */
    fun formatDateForUi(currentDate: String): String {
        val inputDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputDateFormat = SimpleDateFormat("EEE',' dd MMM")
        val date:Date = inputDateFormat.parse(currentDate)!!

        return outputDateFormat.format(date)
    }

    /**
     * returns date as "dd MMM YY"
     */
    fun formatDateForUi(year: Int, month: Int, day: Int): String {

        val currentDate = "$year-$month-$day"

        val inputDateFormat = SimpleDateFormat("yyyy-MM-dd")
        val outputDateFormat = SimpleDateFormat("dd MMM YY")
        val date:Date = inputDateFormat.parse(currentDate)!!

        return outputDateFormat.format(date)
    }
}