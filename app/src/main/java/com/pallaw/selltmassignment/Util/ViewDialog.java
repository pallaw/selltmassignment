package com.pallaw.selltmassignment.Util;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;

import com.pallaw.selltmassignment.R;

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
public class ViewDialog {

    Activity activity;
    Dialog dialog;

    public ViewDialog(Activity activity) {
        this.activity = activity;
    }

    public void showDialog() {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.loading_view);

        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
