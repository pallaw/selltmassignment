package com.pallaw.selltmassignment.UI.Booking

import com.pallaw.selltmassignment.Base.BaseResponse
import com.pallaw.selltmassignment.Base.BaseResponseObserver
import com.pallaw.selltmassignment.Data.ApiServices
import com.pallaw.selltmassignment.Data.DataManager
import com.pallaw.selltmassignment.Data.Model.SearchResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class BookingPresenter(val view: BookingContract.View, val dataManager: DataManager) :
    BookingContract.Presenter {
    override fun searchBus(bookinDate: String) {
        view.toggleLoading(true)
        dataManager.apiService.searchBus(ApiServices.TOKEN, "3", "6", bookinDate)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : BaseResponseObserver<SearchResponse>() {
                override fun onResponse(response: SearchResponse) {
                    view.toggleLoading(false)
                    view.handleSearchResponse(response)
                }

                override fun onServerError(response: BaseResponse) {
                    view.toggleLoading(false)
                    view.showMessage(response.message)
                    val message = response.message
                }
            })
    }
}