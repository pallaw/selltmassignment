package com.pallaw.selltmassignment.UI.Booking

import com.pallaw.selltmassignment.Data.Model.SearchResponse

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
interface BookingContract {
    interface View{
        fun handleSearchResponse(response: SearchResponse)
        fun toggleLoading(enable: Boolean)
        fun showMessage(message: String)
    }
    interface Presenter{
        fun searchBus(bookinDate: String)
    }
}