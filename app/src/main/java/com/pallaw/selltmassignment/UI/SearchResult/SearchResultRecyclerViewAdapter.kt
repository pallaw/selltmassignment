package com.pallaw.selltmassignment.UI.SearchResult

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pallaw.selltmassignment.Data.Model.SearchResponse
import com.pallaw.selltmassignment.R

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class SearchResultRecyclerViewAdapter(val dataList: ArrayList<SearchResponse.Data>) :
    RecyclerView.Adapter<SearchResultRecyclerViewAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var rootview = itemView
        var rupeeSymbol: String = ""
        get() {return rootview.context.getString(R.string.Rs)}

        private var itemSearchTime: TextView = itemView.findViewById(R.id.itemSearchTime)
        private var itemSearchDuration: TextView = itemView.findViewById(R.id.itemSearchDuration)
        private var itemSearchSeatLeft: TextView = itemView.findViewById(R.id.itemSearchSeatLeft)
        private var itemSearchTotalAmount: TextView = itemView.findViewById(R.id.itemSearchTotalAmount)
        private var itemSearchDiscountedPrice: TextView = itemView.findViewById(R.id.itemSearchDiscountedPrice)
        private var itemSearchDiscount: TextView = itemView.findViewById(R.id.itemSearchDiscount)
        private var itemSearchTravelCompany: TextView = itemView.findViewById(R.id.itemSearchTravelCompany)
        private var itemSearchSeatType: TextView = itemView.findViewById(R.id.itemSearchSeatType)

        fun bind(data: SearchResponse.Data) {
            itemSearchTime.text = "${data.arrivalTime}-${data.departureTime}"
            itemSearchDuration.text = "dur (4hr 10mins)"
            itemSearchSeatLeft.text = "${data.availableSeats} seat(s) left"
            itemSearchTotalAmount.text = "${rupeeSymbol} ${data.fareDetailsSafe.get(0).totalFare}"
            itemSearchTravelCompany.text = "${data.travels}"
            itemSearchSeatType.text = "${data.busType}"
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): SearchResultRecyclerViewAdapter.MyViewHolder {
        // create a new view
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_result, parent, false)

        return MyViewHolder(itemView)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataList.get(position))
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataList.size
}