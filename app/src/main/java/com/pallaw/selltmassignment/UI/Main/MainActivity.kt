package com.pallaw.selltmassignment.UI.Main

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.pallaw.selltmassignment.R
import com.pallaw.selltmassignment.UI.Booking.BookingFragment

import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        //show Bus booking Fragment
        loadBookingFragment()
    }

    private fun loadBookingFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.contentMain, BookingFragment.newInstance()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {onBackPressed()}
        }
        return super.onOptionsItemSelected(item)
    }
}
