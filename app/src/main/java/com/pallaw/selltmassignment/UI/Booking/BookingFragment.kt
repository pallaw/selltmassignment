package com.pallaw.selltmassignment.UI.Booking


import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.pallaw.selltmassignment.Data.DataManager
import com.pallaw.selltmassignment.Data.Model.SearchResponse
import com.pallaw.selltmassignment.R
import com.pallaw.selltmassignment.UI.SearchResult.SearchResultFragment
import com.pallaw.selltmassignment.Util.CalenderUtil
import com.pallaw.selltmassignment.Util.ViewDialog
import kotlinx.android.synthetic.main.fragment_booking.*
import java.util.*


/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class BookingFragment : Fragment(), BookingContract.View, View.OnClickListener {

    companion object {
        fun newInstance(bundle: Bundle? = null): BookingFragment {
            val bookingFragment = BookingFragment()
            bookingFragment.arguments = bundle

            return bookingFragment
        }
    }

    private lateinit var loaderDialog: ViewDialog
    private lateinit var rootView: View
    private lateinit var presenter: BookingPresenter
    private lateinit var dataManager: DataManager

    private var selectedCalendar: Calendar = Calendar.getInstance().apply { this.timeZone = TimeZone.getTimeZone("Asia/Calcutta") }
    private val calendarYear: Int get() {return selectedCalendar.get(Calendar.YEAR)}
    private val calendarMonth: Int get() {return selectedCalendar.get(Calendar.MONTH) + 1}
    private val calendarDay: Int get() {return selectedCalendar.get(Calendar.DAY_OF_MONTH)}

    private val bookingDate: String get() {
            return "$calendarYear-$calendarMonth-$calendarDay"
        }
    private val source: String get() {
            return spiBookingSource.selectedItem as String
        }
    private val destination: String get() {
            return spiBookingDestination.selectedItem as String
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_booking, container, false)
        dataManager = DataManager(activity as FragmentActivity)
        presenter = BookingPresenter(this, dataManager)
        loaderDialog = ViewDialog(activity)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //initialize the UI components
        setupUi()

    }

    override fun onResume() {
        super.onResume()
        val supportActionBar = (activity as AppCompatActivity).supportActionBar
        supportActionBar?.title = getString(R.string.title_booking)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
    }

    private fun setupUi() {
        //set current date as default
        txtBookingDate.text = CalenderUtil().formatDateForUi(calendarYear, calendarMonth, calendarDay)

        txtBookingDate.setOnClickListener(this)
        btnBookingSearch.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.txtBookingDate -> {
                txtBookingDate.isClickable = false
                val datePickerDialog = DatePickerDialog(
                    context!!,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        txtBookingDate.isClickable = true
                        txtBookingDate.text = CalenderUtil().formatDateForUi(year, monthOfYear, dayOfMonth)

                        selectedCalendar.set(Calendar.YEAR, year)
                        selectedCalendar.set(Calendar.MONTH, monthOfYear)
                        selectedCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                    }, calendarYear, calendarMonth, calendarDay
                ).apply { this.datePicker.minDate = selectedCalendar.timeInMillis - 1000 }
                datePickerDialog.show()
            }

            R.id.btnBookingSearch -> {
                presenter.searchBus(bookingDate)
            }
        }
    }

    override fun toggleLoading(enable: Boolean) {
        if (enable) loaderDialog.showDialog() else loaderDialog.hideDialog()
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun handleSearchResponse(response: SearchResponse) {
        showMessage(response.message)

        //if status is 200 (OK) then move to search result page
        if (response.status.contentEquals("200")) {
            val toSearchResult = activity?.supportFragmentManager?.beginTransaction()
            val bundle = Bundle().apply {
                this.putSerializable(SearchResultFragment.ARG_DATA, response)
                this.putString(SearchResultFragment.ARG_SELECTED_DATE, bookingDate)
                this.putString(SearchResultFragment.ARG_SOURCE, source)
                this.putString(SearchResultFragment.ARG_DESTINATION, destination)
            }
            toSearchResult?.replace(R.id.contentMain, SearchResultFragment.newInstance(bundle))
            toSearchResult?.addToBackStack(SearchResultFragment.TAG)
            toSearchResult?.commit()
        }
    }
}
