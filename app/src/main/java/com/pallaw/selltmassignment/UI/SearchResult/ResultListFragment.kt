package com.pallaw.selltmassignment.UI.SearchResult


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pallaw.selltmassignment.Base.BaseResponse
import com.pallaw.selltmassignment.Base.BaseResponseObserver
import com.pallaw.selltmassignment.Data.ApiServices
import com.pallaw.selltmassignment.Data.DataManager
import com.pallaw.selltmassignment.Data.Model.SearchResponse

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_result_list.*
import com.pallaw.selltmassignment.R



/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class ResultListFragment : Fragment() {

    private lateinit var adapter: SearchResultRecyclerViewAdapter

    companion object {
        fun newInstance(bundle: Bundle? = null): ResultListFragment {
            val bookingFragment = ResultListFragment()
            bookingFragment.arguments = bundle

            return bookingFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setRetainInstance(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_result_list, container, false)
    }

    var isDataLoaded = false

    private fun loadSearchResults(dateOfTravel: String) {
        val dataManager = DataManager(activity as FragmentActivity)
        dataManager.apiService.searchBus(ApiServices.TOKEN, "3", "6", dateOfTravel)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : BaseResponseObserver<SearchResponse>() {
                override fun onResponse(response: SearchResponse) {
                    showResultList(response.dataSafe)
                }

                override fun onServerError(response: BaseResponse) {

                }
            })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var data: SearchResponse? = arguments?.getSerializable(SearchResultFragment.ARG_DATA) as SearchResponse?
        var dateOfTravel: String = arguments!!.getString(SearchResultFragment.ARG_SELECTED_DATE)!!

        if (null == data) {
            if (!isDataLoaded) {
                loadSearchResults(dateOfTravel)
                isDataLoaded = true
            }
        } else {
            var data: SearchResponse = arguments!!.getSerializable(SearchResultFragment.ARG_DATA)!! as SearchResponse
            showResultList(data.dataSafe)
        }
    }

    private fun showResultList(dataList: ArrayList<SearchResponse.Data>) {

        resultListRv.setHasFixedSize(true)
        resultListRv.layoutManager = LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false)
        resultListRv.setHasFixedSize(true)
        resultListRv.itemAnimator = DefaultItemAnimator()
        adapter = SearchResultRecyclerViewAdapter(dataList)
        resultListRv.adapter = adapter
    }

}
