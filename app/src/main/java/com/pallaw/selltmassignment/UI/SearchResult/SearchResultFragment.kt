package com.pallaw.selltmassignment.UI.SearchResult


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.pallaw.selltmassignment.R
import com.pallaw.selltmassignment.Util.CalenderUtil
import kotlinx.android.synthetic.main.fragment_search_result.*

/**
 * Created by Pallaw Pathak on 2019-09-29. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class SearchResultFragment : Fragment() {

    private lateinit var destination: String
    private lateinit var source: String
    private lateinit var selectedDate: String

    companion object {
        val TAG: String = SearchResultFragment::class.java.simpleName
        val ARG_SOURCE: String = "arg_sourc"
        val ARG_DESTINATION: String = "arg_destination"
        val ARG_DATA: String = "arg_data"
        val ARG_SELECTED_DATE: String = "arg_selected_date"

        fun newInstance(bundle: Bundle? = null): SearchResultFragment {
            val bookingFragment = SearchResultFragment()
            bookingFragment.arguments = bundle

            return bookingFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    override fun onResume() {
        super.onResume()
        val supportActionBar = (activity as AppCompatActivity).supportActionBar
        supportActionBar?.title = "${source} to ${destination}"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var adapter = TabAdapter(fragmentManager)

        selectedDate = arguments!!.getString(ARG_SELECTED_DATE)!!
        source = arguments!!.getString(ARG_SOURCE)!!
        destination = arguments!!.getString(ARG_DESTINATION)!!

        adapter.addFragment(ResultListFragment.newInstance(arguments), CalenderUtil().formatDateForUi(selectedDate))
        for (i in 1..6) {
            val incrementedDate = CalenderUtil().getIncrementedDate(selectedDate, i)
            var bundle = Bundle().apply { this.putString(SearchResultFragment.ARG_SELECTED_DATE,
                incrementedDate
            ) }
            adapter.addFragment(ResultListFragment.newInstance(bundle), CalenderUtil().formatDateForUi(incrementedDate))
        }
        searchResultViewPager.offscreenPageLimit = 7// show 1 week data
        searchResultViewPager.adapter = adapter
        searchResultTabs.setupWithViewPager(searchResultViewPager)
    }

}
